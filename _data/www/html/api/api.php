<?php
/*******************************************************
 * Marcel Braun             Created at: 21.06.19
 * marcel@fstack.de         Filename:   api.php
 *
 * Description:
 * Rest API mit dem SlimFramework
 *
 *******************************************************/

use API\Classes\ResponseObject;

require_once "middleware.php";

foreach (glob(__DIR__ . "/classes/*.php") as $filename)
{
    include $filename;
}

/*
 _____                    _   _
| ____|_  _____ ___ _ __ | |_(_) ___  _ __
|  _| \ \/ / __/ _ \ '_ \| __| |/ _ \| '_ \
| |___ >  < (_|  __/ |_) | |_| | (_) | | | |
|_____/_/\_\___\___| .__/ \__|_|\___/|_| |_|
                   |_|
 _   _                 _ _
| | | | __ _ _ __   __| | | ___ _ __
| |_| |/ _` | '_ \ / _` | |/ _ \ '__|
|  _  | (_| | | | | (_| | |  __/ |
|_| |_|\__,_|_| |_|\__,_|_|\___|_|
 */
$c = new \Slim\Container();
$c['errorHandler'] = function ($c) {
    return function ($request, $response, $exception) use ($c) {
        return $response->withStatus(400)
            ->withJSON(new ResponseObject(NULL, NULL, $exception->getMessage()));
    };
};
$c['phpErrorHandler'] = function ($c) {
    return function ($request, $response, $exception) use ($c) {
        return $response->withStatus(500)
            ->withJSON(new ResponseObject(NULL, NULL, $exception->getMessage()));
    };
};

# Init app
$app = new \Slim\App($c);
$app->add(new \API\Auth());

include "routes.php";

# Start the App
$app->run();

exit;