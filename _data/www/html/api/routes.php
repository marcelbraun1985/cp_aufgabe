<?php
/*******************************************************
 * Marcel Braun             Created at: 22.06.19
 * marcel@fstack.de         Filename:   routes.php
 *
 * Description:
 *
 *
 *******************************************************/

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use API\Classes as c;

$app->post('/registration', function (Request $request, Response $response, array $args) {
    return c\Registrierung::registration($request, $response, $args);
});

$app->get('/confirmation', function (Request $request, Response $response, array $args) {
    return c\Registrierung::confirmation($request, $response, $args);
});

$app->post('/login', function (Request $request, Response $response, array $args) {
    return c\Anmeldung::login($request, $response, $args);
});

$app->get('/validate', function (Request $request, Response $response, array $args) {
    return c\Anmeldung::validate($request, $response, $args);
});

$app->post('/reset', function (Request $request, Response $response, array $args) {
    return c\Anmeldung::askForPasswordReset($request, $response, $args);
});

$app->get('/reset2',function (Request $request, Response $response, array $args) {
    return c\Anmeldung::resetPassword($request, $response, $args);
});

$app->get('/user', function (Request $request, Response $response, array $args) {
    return c\Verwaltung::user($request, $response, $args);
});

$app->post('/rename', function (Request $request, Response $response, array $args) {
    return c\Verwaltung::rename($request, $response, $args);
});

$app->post('/delete', function (Request $request, Response $response, array $args) {
    return c\Verwaltung::deleteUser($request, $response, $args);
});

$app->post('/changePassword', function (Request $request, Response $response, array $args) {
    return c\Verwaltung::changePassword($request, $response, $args);
});

