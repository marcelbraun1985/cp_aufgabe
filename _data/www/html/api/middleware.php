<?php
/*******************************************************
 * Marcel Braun             Created at: 22.06.19
 * marcel@fstack.de         Filename:   middleware.php
 *
 * Description:
 * Slim Framework Middleware
 *
 *******************************************************/

namespace API;

use API\Classes\Helper;
use Psr\Http\Message\ResponseInterface;
use MB\Classes\Anmeldung;
use API\Classes\ResponseObject;

/**
 * Class Auth

 * Add the following at the beginning of each route that have to use Authentication
 * @example if(!$request->getAttribute("isTokenValid")) return $response;
 */
Class Auth {
    public function __invoke ($request, $response, $next) : ResponseInterface
    {
        global $config;

        $anmeldung = new Anmeldung($config);
        $validation = false;

        if(Helper::getToken()) {
            $validation = $anmeldung->Ist_Anmeldung_gueltig(Helper::getToken());

            if(!$validation) {
                $response = $response
                    ->withStatus(401)
                    ->withJSON(new ResponseObject(NULL, NULL, "NOT_AUTHORIZED"));
            }
        }

        $request = $request->withAttribute('isTokenValid', $validation);

        $response = $next($request, $response);

        return $response;
    }
}