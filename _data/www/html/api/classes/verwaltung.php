<?php
/*******************************************************
 * Marcel Braun             Created at: 22.06.19
 * marcel@fstack.de         Filename:   verwaltung.php
 *
 * Description:
 *
 *
 *******************************************************/

namespace API\Classes;

use MB\Classes as c;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class Verwaltung
{
    static public function user(Request $request, Response $response, array $args): Response
    {
        global $config;

        $verwaltung = new c\Verwaltung($config);
        $user = $verwaltung->Aktueller_Benutzer(Helper::getToken());

        return $response->withJSON(new ResponseObject($user));
    }

    static public function rename(Request $request, Response $response, array $args): Response
    {
        global $config;

        if (!$request->getAttribute("isTokenValid")) return $response;

        $verwaltung = new c\Verwaltung($config);
        $user = $verwaltung->Aktueller_Benutzer(Helper::getToken());

        $verwaltung->Umbenennen($user->Id, $_POST["email"], $_POST["nickname"]);

        return $response->withJSON(new ResponseObject(NULL, "RENAME_SUCCESS"));
    }

    static public function changePassword(Request $request, Response $response, array $args): Response
    {
        global $config;

        if (!$request->getAttribute("isTokenValid")) return $response;

        $verwaltung = new c\Verwaltung($config);
        $user = $verwaltung->Aktueller_Benutzer(Helper::getToken());

        $verwaltung->Passwort_aendern($user->Id, $_POST["password"]);

        return $response->withJSON(new ResponseObject(NULL, "PASSWORD_CHANGED"));
    }

    static public function deleteUser(Request $request, Response $response, array $args): Response
    {
        global $config;

        if (!$request->getAttribute("isTokenValid")) return $response;

        $verwaltung = new c\Verwaltung($config);
        $user = $verwaltung->Aktueller_Benutzer(Helper::getToken());

        $verwaltung->Loeschen($user->Id, $_POST["password"]);

        return $response->withJSON(new ResponseObject(NULL, "USER_DELETED"));
    }
}