<?php
/*******************************************************
 * Marcel Braun             Created at: 22.06.19
 * marcel@fstack.de         Filename:   registrierung.php
 *
 * Description:
 *
 *
 *******************************************************/

namespace API\Classes;

use MB\Classes;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class Registrierung {
    static public function registration(Request $request, Response $response, array $args) : Response {
        global $config;

        $registrierung = new Classes\Registrierung($config);
        $registrierung->registrieren($_POST["email"], $_POST["passwort"], $_POST["nickname"]);
        $message = "REGISTRATION_SUCCESS";

        return $response->withJSON(new ResponseObject(NULL, "REGISTRATION_SUCCESS"));

    }

    static public function confirmation(Request $request, Response $response, array $args) : Response {
        global $config;

        $registrierung = new Classes\Registrierung($config);
        $registrierung->Bestaetigen($_GET["c_token"]);

        return $response->withJSON(new ResponseObject(NULL, "CONFIRMATION_SUCCESS"));
    }
}