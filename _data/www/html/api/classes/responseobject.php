<?php
/*******************************************************
 * Marcel Braun             Created at: 21.06.19
 * marcel@fstack.de         Filename:   responseobject.php
 *
 * Description:
 * Object definition for the JSON response
 *
 *******************************************************/

namespace API\Classes;

class ResponseObject {
    private static $error;
    public static $data;
    private static $message;

    public function __construct($data = [], $messsage = "", $error = NULL)
    {
        $this->data = $data;
        $this->message = $messsage;
        $this->error = $error;
    }

    public function setError(string $value) : void {
        $this->error = $value;
    }

    public function setData(iterable $value) : void {
        $this->data = $value;
    }

    public function setMessage(string $value) : void {
        $this->message = $value;
    }

    public function getJSON() : string {
        return json_encode($this);
    }

    public function __get($key) {
        return $this->{$key};
    }
}