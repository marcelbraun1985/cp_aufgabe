<?php
/*******************************************************
 * Marcel Braun             Created at: 22.06.19
 * marcel@fstack.de         Filename:   anmeldung.php
 *
 * Description:
 *
 *
 *******************************************************/

namespace API\Classes;

use MB\Classes as c;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use API\Classes;

class Anmeldung  {
    static public function login(Request $request, Response $response, array $args) : Response {
        global $config;

        $login = new c\Anmeldung($config);
        $data["token"] = $login->Anmelden($_POST["nickname"], $_POST["password"]);

        return $response->withJSON(new ResponseObject($data));

    }

    static public function validate(Request $request, Response $response, array $args) : Response {
        global $config;

        $anmeldung = new c\Anmeldung($config);
        $validate = $anmeldung->Ist_Anmeldung_gueltig(Helper::getToken());

        if(!$validate)
            Throw new \Exception("INVALID_TOKEN");

        return $response->withJSON(new ResponseObject(NULL, "VALID_TOKEN"));
    }

    static public function askForPasswordReset(Request $request, Response $response, array $args) : Response {
        global $config;

        $anmeldung = new c\Anmeldung($config);
        $anmeldung->Ruecksetzung_des_Passworts_beantragen($_POST["email"]);

        return $response->withJSON(new ResponseObject(NULL, "EMAIL_SENT"));
    }

    static public function resetPassword(Request $request, Response $response, array $args) : Response {
        global $config;

        if(!$request->getAttribute("isTokenValid")) return $response;

        $anmeldung = new c\Anmeldung($config);
        $anmeldung->Passwort_zuruecksetzen($_GET["email"]);

        return $response->withJSON(new ResponseObject(NULL, "EMAIL_SENT"));
    }
}