<?php
use PHPMailer\PHPMailer\PHPMailer;
use MB\Models\Config;

/*
 ____  _   _ ____  __  __       _ _              ____             __ _
|  _ \| | | |  _ \|  \/  | __ _(_) | ___ _ __   / ___|___  _ __  / _(_) __ _
| |_) | |_| | |_) | |\/| |/ _` | | |/ _ \ '__| | |   / _ \| '_ \| |_| |/ _` |
|  __/|  _  |  __/| |  | | (_| | | |  __/ |    | |__| (_) | | | |  _| | (_| |
|_|   |_| |_|_|   |_|  |_|\__,_|_|_|\___|_|     \____\___/|_| |_|_| |_|\__, |
                                                                       |___/
 */
$mail = new PHPMailer(true);
$mail->IsSMTP();
$mail->isHTML(true);
$mail->CharSet = 'UTF-8';
$mail->Host = "smtp.mailtrap.io";
$mail->SMTPDebug = 0;
$mail->SMTPAuth = true;
$mail->Port = 2525;
$mail->Username = "84d5eca0cf98b6";
$mail->Password = "94c2acc8b0dcc9";
$mail->From = "demo@cp.de";

/*
 __  __ ____     _                _          ____             __ _
|  \/  | __ )   | |    ___   __ _(_)_ __    / ___|___  _ __  / _(_) __ _
| |\/| |  _ \   | |   / _ \ / _` | | '_ \  | |   / _ \| '_ \| |_| |/ _` |
| |  | | |_) |  | |__| (_) | (_| | | | | | | |__| (_) | | | |  _| | (_| |
|_|  |_|____/___|_____\___/ \__, |_|_| |_|  \____\___/|_| |_|_| |_|\__, |
           |_____|          |___/                                  |___/
 */
$config = new Config();
/** @var string CallbackURL - without slash */
$config->confirmationCallbackURL = "http://localhost/confirmation";
$config->resetCallbackURL = "http://localhost/reset2";
$config->resetURL = "http://vue/reset";
/** @var PHPMailer mail */
$config->mail = $mail;
$config->db_provider = "mysql";
$config->db_dsn = "mysql:host=db;dbname=cp";
$config->db_user = "root";
$config->db_pass = "pass";
