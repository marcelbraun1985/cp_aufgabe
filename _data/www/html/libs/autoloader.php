<?php
/*******************************************************
 * Marcel Braun             Created at: 21.06.19
 * marcel@fstack.de         Filename:   autoloader.php
 *
 * Description:
 * This function loads all classes if they are needed. So it
 * is not necessary to include the classes individually.
 *
 *******************************************************/

/**
 * @param $class_name
 */
function autoload($class_name)
{
    $file = __DIR__ . "/" . strtolower(str_replace('\\', '/', $class_name)) . '.php';

    if (file_exists($file)) {
        require_once($file);
    } else {
        echo $file;
    }
}

spl_autoload_register('autoload');