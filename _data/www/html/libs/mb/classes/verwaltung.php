<?php
/*******************************************************
 * Marcel Braun             Created at: 21.06.19 
 * marcel@fstack.de         Filename:   verwaltung.php          
 * 
 * Description:
 *
 *
 *******************************************************/
 
namespace MB\Classes;

use MB\Interfaces\IVerwaltung;
use MB\Models\Benutzer;

class Verwaltung extends Core implements IVerwaltung
{
    public function Aktueller_Benutzer(string $token) : Benutzer {
        return $this->db->getUserByToken($token);
    }

    public function Umbenennen(string $benutzerId, string $email, string $nickname): void {
        $this->db->renameUser($benutzerId, $email, $nickname);
    }

    public function Passwort_aendern(string $benutzerId, string $passwort): void {
        $this->db->changePassword($benutzerId, $passwort);
    }

    public function Loeschen(string $benutzerId, string $passwort): void {
        $this->db->deleteUser($benutzerId, $passwort);
    }
}