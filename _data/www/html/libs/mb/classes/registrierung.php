<?php
/*******************************************************
 * Marcel Braun             Created at: 21.06.19
 * marcel@fstack.de         Filename:   registrierung.php
 *
 * Description:
 * This class handles the registration process.
 *
 *******************************************************/

namespace MB\Classes;

use MB\Interfaces\IRegistrierung;
use PHPMailer\PHPMailer\Exception;

class Registrierung extends Core implements IRegistrierung
{
    public function Registrieren(string $email, string $passwort, string $nickname): void
    {
        Validations::email($email);
        Validations::password($passwort);
        Validations::nickname($nickname);

        # Token wird generiert
        $token = $this->generateToken();

        # Neuer User wird der Datenbank hinzugefügt
        $this->db->insertUser($email,  $passwort, $nickname, $token);

        # Bestätigungslink für die E-Mail wird generiert
        $confirmation_link = $this->config->confirmationCallbackURL . "?c_token=$token";

        # Templates werden geladen
        $html = $this->getHtmlText($nickname, $confirmation_link);
        $altbody = $this->getAltText($nickname, $confirmation_link);

        # E-Mail wird gesendet
        $this->sendMail($email, "Registrierung" ,$html, $altbody);
    }

    public function Bestaetigen(string $registrierungsnummer): void
    {
        if(!$this->db->checkIfUserIsActive($registrierungsnummer)) {
            $this->db->activateUser($registrierungsnummer);
        } else {
            throw new \Exception("WRONG_TOKEN");
        }
    }

    private function getHtmlText(string $nickname, string $confirmation_link) : string {
        return <<<EOF
            <p>Hi $nickname,</p><p>vielen Dank für Deine Registrierung!</p>
            <p>Bitte bestätige deine Registrierung mit dem folgendem Link:<br>
            <a href="$confirmation_link">$confirmation_link</a></p>
EOF;
    }

    private function getAltText(string $nickname, string $confirmation_link) : string {
        return <<<EOF
            Hi $nickname,\n\n
            vielen Dank für deine Registrierung!\n\n
            Bitte bestätige deine Registrierung mit dem folgendem Link:\n
            $confirmation_link
EOF;
    }
}