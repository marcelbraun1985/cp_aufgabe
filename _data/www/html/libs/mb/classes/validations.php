<?php
/*******************************************************
 * Marcel Braun             Created at: 21.06.19
 * marcel@fstack.de         Filename:   validations.php
 *
 * Description:
 *
 *
 *******************************************************/

namespace MB\Classes;

class Validations {

    /**
     * Die Methode func() wirft eine Exception sofern eines der übergebenen Argumente
     * gleich NULL ist. Die Funktion überprüft von welcher Klasse und Methode der Aufruf
     * kam und ermittelt die Bezeichnung der einzelen Argumente um dem Programmierer anzuzeigen
     * welche Argumente er vergessen haben könnte zu implementieren.
     *
     * Damit diese Funktion genutzt werden kann, müssen alle Argumente einer Methode den default
     * Wert NULL aufweisen.
     *
     * @example Validations::func(func_get_args());
     *
     * @throws \ReflectionException
     * @param array $args;
     */
    static public function func(array $args) : void {
        $missing_args = false;
        foreach ($args as $v) {
            if($v == NULL) {
                $missing_args = true;
                break;
            }
        }

        $message = "";
        if($missing_args) {
            $class = debug_backtrace()[1]['class'];
            $method = debug_backtrace()[1]['function'];

            $ReflectionMethod =  new \ReflectionMethod($class, $method);
            $params = $ReflectionMethod->getParameters();

            foreach ($params as $param) {
                $message .= "$param->name, ";
            }

            $message = substr($message, 0, -2);
            $message = "WRONG_ARGUMENTS: $message";

            throw new \Exception($message);
        }
    }

    static public function email($email) : void {
        if(!filter_var($email, FILTER_VALIDATE_EMAIL))
            throw new \Exception("EMAIL_NOT_VALID");
    }

    static public function password($password) : void {
        # TODO: implement password validation
    }

    static public function nickname($nickname) : void {
        # TODO: implement nickname validation
    }

    static public function Token($token) : void {
        # TODO: implement Token validation
    }
}