<?php
/*******************************************************
 * Marcel Braun             Created at: 21.06.19 
 * marcel@fstack.de         Filename:   core.php          
 * 
 * Description:
 * Programming class for inheritance.
 *
 *******************************************************/

namespace MB\Classes;
use MB\Helper;
use MB\Models\Config;
use MB\Provider\MySql;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;

class Core
{
    /** @var PHPMailer PHPMailer */
    private $phpMailer;
    /** @var MySql $db */
    public $db;
    /** @var Config $config */
    public $config;

    public function __construct(Config $config)
    {
        $this->phpMailer = $config->mail;
        $this->config = $config;

        try {
            $provider_class = "\\MB\\Provider\\" . $config->db_provider;
            $this->db = new $provider_class($config->db_dsn, $config->db_user, $config->db_pass);

        } catch (\PDOException $e) {
            # überschreibt die default message um dem viewer keine Infos über den MySQL User mitzuteilen.
            throw new \PDOException("NO_DB_CONNECTION");
        }
    }

    public function sendMail(string $to, string $subject, string $body, string $altbody = "") : bool
    {
        try {
            $mail = $this->phpMailer;
            $mail->addAddress($to);
            $mail->Subject = $subject;
            $mail->Body = $body;
            $mail->AltBody = $altbody;

            $mail->send();

            return true;

        } catch (Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    # TODO: Deprecated
    public function generateToken() :string {
        return Helper::generateToken();
    }
}