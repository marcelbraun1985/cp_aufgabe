<?php
/*******************************************************
 * Marcel Braun             Created at: 21.06.19 
 * marcel@fstack.de         Filename:   anmeldung.php          
 * 
 * Description:
 *
 *
 *******************************************************/

namespace MB\Classes;

use MB\Helper;
use MB\Interfaces\IAnmeldung;

class Anmeldung extends Core implements IAnmeldung
{
    /**
     * @param string $anmeldename
     * @param string $passwort
     * @return string (Token)
     * @throws \Exception
     */
    public function Anmelden(string $anmeldename, string $passwort): string
    {
        # Gibt den Token des Users zurück
        return $this->db->login($anmeldename, $passwort);
    }

    /**
     * @param string $token
     * @return bool
     */
    public function Ist_Anmeldung_gueltig(string $token): bool
    {
        return $this->db->validateToken($token);
    }

    public function Ruecksetzung_des_Passworts_beantragen(string $email): void
    {
        $user = $this->db->getUserByEmail($email);
        $token = $this->generateToken();
        $this->db->setNewToken($user->Id, $token);

        $reset_link = "{$this->config->resetCallbackURL}?email=$email&token=$token";

        $this->sendMail($email, "Passwort zurücksetzen",
            $this->getHtmlText($user->Nickname, $reset_link),
            $this->getAltText($user->Nickname, $reset_link));
    }

    /**
     * Erstellt ein zufälliges Passwort, speichert es als Hash in der Datenbank und sendet das
     * neue Password dem User per Email zu. Nach der Anmeldung kann er dann über die Verwaltung
     * ein eigenes erstellen.
     *
     * @param string $email
     * @throws \Exception
     */
    public function Passwort_zuruecksetzen(string $email): void
    {
        $user = $this->db->getUserByEmail($email);

        # TODO: Implement one time password
        $verwaltug = new Verwaltung($this->config);
        $bytes = random_bytes(10);
        $new_password = bin2hex($bytes);
        $new_hash = Helper::generateHash($new_password);

        $verwaltug->Passwort_aendern($user->Id, $new_hash);

        $message = "Neues Passwort: $new_password";

        $this->sendMail($email, "Password zurückgesetzt", $message, $message);
    }


    private function getHtmlText(string $nickname, string $reset_link) : string {
        return <<<EOF
            <p>Hi $nickname, Hast du beantragt dein Password zurück zu setzen?</p>
            <p>Dann klicke auf den unten stehenden Link um dein Passwort zurück zu setzen:<br>
            <a href="$reset_link">$reset_link</a></p>
            <p>Solltest du das Passwort zurücksetzen nicht beantrag haben, so kannst du diese
            Email einfach ignorieren.</p>
EOF;
    }

    private function getAltText(string $nickname, string $reset_link) : string {
        return <<<EOF
            Hi $nickname,\n\n
            Passwort zurücksetzen:
            $reset_link
EOF;
    }
}