<?php
/*******************************************************
 * Marcel Braun             Created at: 21.06.19 
 * marcel@fstack.de         Filename:   iverwaltung.php          
 * 
 * Description:
 *
 *
 *******************************************************/
 
namespace MB\Interfaces;

use MB\Models\Benutzer;

interface IVerwaltung
{
    public function Aktueller_Benutzer(string $token): Benutzer;

    public function Umbenennen(string $benutzerId, string $email, string $nickname): void;

    public function Passwort_aendern(string $benutzerId, string $passwort): void;

    public function Loeschen(string $benutzerId, string $passwort): void;
}
