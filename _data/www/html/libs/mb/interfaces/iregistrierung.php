<?php
/*******************************************************
 * Marcel Braun             Created at: 21.06.19 
 * marcel@fstack.de         Filename:   iregistrierung.php          
 * 
 * Description:
 *
 *
 *******************************************************/
 
namespace MB\Interfaces;

interface IRegistrierung
{
    public function Registrieren(string $email, string $passwort, string $nickname): void;

    public function Bestaetigen(string $registrierungsnummer): void;
}