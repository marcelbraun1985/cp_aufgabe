<?php
/*******************************************************
 * Marcel Braun             Created at: 21.06.19 
 * marcel@fstack.de         Filename:   idatenbank.php          
 * 
 * Description:
 *
 *
 *******************************************************/
 
namespace MB\Interfaces;

use MB\Models\Benutzer;

# TODO: Ist noch nicht komplett
interface IDatenbank
{
    public function __construct(string $dsn, string $username, string $passwd);

    public function insertUser($email, $passwort, $nickname, $token): void;

    public function checkIfUserIsActive($registrierungsnummer): bool;

    public function activateUser($registrierungsnummer): void;

    /** @return string (Token) */
    public function login(string $nickname, string $password): string;

    public function validateToken(string $token) : bool;

    public function getUserByToken(string $token) : Benutzer;

    public function getUserByEmail(string $email) : Benutzer;
}