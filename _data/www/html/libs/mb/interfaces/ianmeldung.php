<?php
/*******************************************************
 * Marcel Braun             Created at: 21.06.19 
 * marcel@fstack.de         Filename:   ianmeldung.php          
 * 
 * Description:
 *
 *
 *******************************************************/
 
namespace MB\Interfaces;

interface IAnmeldung
{
    public function Anmelden(string $anmeldename, string $passwort): string;

    public function Ist_Anmeldung_gueltig(string $token): bool;

    public function Ruecksetzung_des_Passworts_beantragen(string $email): void;

    public function Passwort_zuruecksetzen(string $email): void;
}