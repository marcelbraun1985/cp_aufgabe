<?php
/*******************************************************
 * Marcel Braun             Created at: 21.06.19
 * marcel@fstack.de         Filename:   helper.php
 *
 * Description:
 * Helper class with some useful methods.
 *
 *******************************************************/

namespace MB;

class Helper {
    /**
     * @param string $string
     * @return string md5
     */
    static public function generateHash(string $string): string
    {
        return md5($string);
    }

    static public function generateToken() : string {
        return bin2hex(random_bytes(32));
    }

    /**
     * Uses DateTime to generate a formatted time string in specified format.
     *
     * @param string $time
     * @param string $format
     * @return string
     */
    static public function getFormattedDateTime(string $time = "NOW", string $format = "Y-m-d H:i:s"): string
    {
        $date = new \DateTime($time, new \DateTimeZone("Europe/Berlin"));
        return $date->format($format);
    }
}