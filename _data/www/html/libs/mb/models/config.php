<?php
/*******************************************************
 * Marcel Braun             Created at: 21.06.19 
 * marcel@fstack.de         Filename:   config.php          
 * 
 * Description:
 *
 *
 *******************************************************/
 
namespace MB\Models;

class Config {
    /** @var string CallbackURL - without slash */
    public $confirmationCallbackURL;
    public $resetCallbackURL;
    public $resetURL;
    /** @var PHPMailer $mail */
    public $mail;
    /** @var \PDO dsn $db_dsn */
    public $db_dsn;
    public $db_user;
    public $db_pass;
    public $db_provider;
}