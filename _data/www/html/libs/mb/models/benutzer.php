<?php
/*******************************************************
 * Marcel Braun             Created at: 21.06.19 
 * marcel@fstack.de         Filename:   benutzer.php          
 * 
 * Description:
 *
 *
 *******************************************************/
 
Namespace MB\Models;
/**
 * Class Benutzer
 *
 *
 */
class Benutzer
{
    /** @var string */
    public $Id;
    /** @var string */
    public $Email;
    /** @var string */
    public $Nickname;
    /** @var DateTime */
    public $Registrierungsdatum;
    /** @var DateTime */
    public $LetzteAnmeldung;
    /** @var DateTime */
    public $LetzteAktualisierung;

    public $Token;
    public $Active;

    public function __construct()
    {

    }

    /*
    public function __set($name, $value)
    {
        $this->{$name} = $value;
    }

    public function __get($name)
    {
        return $this->{$name};
    }
    */
}