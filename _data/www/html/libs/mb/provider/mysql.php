<?php
/*******************************************************
 * Marcel Braun             Created at: 21.06.19
 * marcel@fstack.de         Filename:   mysql.php
 *
 * Description:
 * MySQL Provider with PDO.
 *
 *******************************************************/

namespace MB\Provider;

use MB\Interfaces\IDatenbank;
use MB\Models\Benutzer;
use MB\Helper;

class MySql extends \PDO implements IDatenbank
{
    # TODO: Add parameter $host and use a fixed dsn
    public function __construct(string $dsn, string $username, string $passwd)
    {
        parent::__construct($dsn, $username, $passwd);

        # Enable error reporting for PDO
        $this->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

    }

    /**
     * @return object
     */
    private function statements(): object
    {
        $statements = [
            "GET_USER_BY_EMAIL" => "SELECT * FROM users WHERE email = ? OR nickname = ?",
            "GET_USER_BY_ID_AND_PASSWORD" => "SELECT * FROM users WHERE Id = :id AND Passwort = :pass",
            "INSERT_USER" => "INSERT INTO users (Email, Passwort, Nickname, Token, Registrierungsdatum) VALUES (?,?,?,?,?)",
            "GET_USER_BY_TOKEN" => "SELECT * FROM users WHERE Token = ?",
            "GET_USER_BY_TOKEN_AND_NOT_ACTIVE" => "SELECT * FROM users WHERE token=? and active = 0",
            "UPDATE_USER_SET_ACTIVE" => "UPDATE users SET active=1 WHERE token=?",
            "LOGIN" => "SELECT * FROM users WHERE Nickname = ? AND Passwort = ? OR Email = ? AND Passwort = ?",
            "UPDATE_LETZTE_ANMELDUNG" => "UPDATE users SET LetzteAnmeldung = ? WHERE Token = ?",
            "UPDATE_TOKEN" => "UPDATE users SET Token = :token WHERE Id = :id",
            "VALIDATE_TOKEN" => "SELECT Token FROM users WHERE Token = ?",
            "CHANGE_PASSWORD" => "UPDATE users SET Passwort = :pass WHERE Id = :id",
            "DELETE_USER" => "DELETE FROM users WHERE Id = :id AND Passwort = :pass",
            "RENAME_USER" => "UPDATE users SET Email = :email, Nickname = :nickname WHERE Id = :id",
        ];

        return (object)$statements;
    }

    /**
     * @param $email
     * @return bool
     */
    public function checkIfUserExists($login, $email): bool
    {
        $stmt = $this->prepare($this->statements()->GET_USER_BY_EMAIL);
        $stmt->execute(array($login, $email));

        return ($stmt->rowCount() > 0) ? true : false;
    }

    /**
     * The password must be md5 encrypted.
     *
     * @param $email
     * @param $passwort
     * @param $nickname
     * @param $token
     * @throws \Exception
     */
    public function insertUser($email, $passwort, $nickname, $token): void
    {
        if (!$this->checkIfUserExists($email, $nickname)) {
            $stmt = $this->prepare($this->statements()->INSERT_USER);
            $stmt->execute(array($email, Helper::generateHash($passwort), $nickname, $token, Helper::getFormattedDateTime()));
        } else {
            throw new \Exception("USER_EXISTS");
        }
    }

    /**
     * @param $registrierungsnummer
     * @return int
     */
    public function checkIfUserIsActive($token): bool
    {
        $stmt = $this->prepare($this->statements()->GET_USER_BY_TOKEN_AND_NOT_ACTIVE);
        $stmt->execute(array($token));

        return ($stmt->rowCount() > 0) ? false : true;
    }

    /**
     * @param $registrierungsnummer
     */
    public function activateUser($registrierungsnummer): void
    {
        $stmt = $this->prepare($this->statements()->UPDATE_USER_SET_ACTIVE);
        $stmt->execute(array($registrierungsnummer));
    }

    /**
     * @param $nickname
     * @param $password
     * @return string Token
     * @throws \Exception
     */
    public function login(string $login, string $password): string
    {
        $hash = Helper::generateHash($password);
        $stmt = $this->prepare($this->statements()->LOGIN);
        $stmt->execute(array($login, $hash, $login, $hash));

        $user = $this->getUser($stmt);

        # Update DateTime field LetzteAktualisierung
        $stmt = $this->prepare($this->statements()->UPDATE_LETZTE_ANMELDUNG);
        $stmt->execute(array(Helper::getFormattedDateTime(), $user->Token));

        $new_token = Helper::generateToken();
        $this->setNewToken($user->Id, $new_token);

        return $new_token;
    }

    public function setNewToken(int $id, string $token): void
    {
        $stmt = $this->prepare($this->statements()->UPDATE_TOKEN);
        $stmt->bindParam("id", $id, \PDO::PARAM_INT);
        $stmt->bindParam("token", $token);
        $stmt->execute();
    }

    /**
     * @param string $token
     * @return bool
     */
    public function validateToken(string $token): bool
    {
        $stmt = $this->prepare($this->statements()->VALIDATE_TOKEN);
        $stmt->execute(array($token));

        return ($stmt->rowCount() > 0) ? true : false;
    }

    /**
     * @param string $token
     * @return Benutzer
     * @throws \Exception
     */
    private function getUser(\PDOStatement $stmt): Benutzer
    {
        if ($stmt->rowCount() > 0) {
            $user = new Benutzer();

            # Datenbank Ergebnis wird dem Benutzer Objekt hinzugefügt
            foreach ($stmt->fetch(\PDO::FETCH_ASSOC) as $k => $v) {
                $user->{$k} = $v;
            }

            return $user;
        } else {
            throw new \Exception("NO_USER");
        }
    }

    public function getUserByToken($token): Benutzer
    {
        $stmt = $this->prepare($this->statements()->GET_USER_BY_TOKEN);
        $stmt->execute(array($token));

        return $this->getUser($stmt);
    }

    public function getUserByEmail($email): Benutzer
    {
        $stmt = $this->prepare($this->statements()->GET_USER_BY_EMAIL);
        $stmt->execute(array($email, $email));

        return $this->getUser($stmt);
    }

    public function changePassword(int $benutzerId, string $password): void
    {
        $password = Helper::generateHash($password);
        $stmt = $this->prepare($this->statements()->CHANGE_PASSWORD);
        $stmt->bindParam("id", $benutzerId, \PDO::PARAM_INT);
        $stmt->bindParam("pass", $password);
        $stmt->execute();
    }

    # TODO: Query not working!
    public function deleteUser(int $benutzerId, string $password): void
    {
        $password = Helper::generateHash($password);

        # Delete the User
        $stmt = $this->prepare($this->statements()->DELETE_USER);
        $stmt->bindParam("id", $benutzerId, \PDO::PARAM_INT);
        $stmt->bindParam("pass", $password);
        $stmt->execute();

        if($stmt->rowCount() == 0) {
            throw new \Exception("WRONG_CREDENTIALS");
        }

    }

    public function renameUser(int $benutzerId, string $email, string $nickname): void
    {
        $stmt = $this->prepare($this->statements()->RENAME_USER);
        $stmt->execute(array($benutzerId, $email, $nickname));
        $stmt->bindParam("id", $benutzerId, \PDO::PARAM_INT);
        $stmt->bindParam("email", $email);
        $stmt->bindParam("nickname", $nickname);
        $stmt->execute();
    }
}