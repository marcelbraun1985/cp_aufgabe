# README

## Anmerkungen

Das eigentliche Projekt ist unter `_data/www/html/libs/mb/` zu
finden und nutzt den Namespace MB. Die Klassen und
Interfaces sind geordnet und können somit
durch meinen Autoloader geladen werden.

Der Autoloader ist in der `boostrap.php` zu finden
welcher zuletzt auch die `config.php` lädt.

Die Lib MB benötigt PHPMailer welche ich über Composer
installiert habe. Ansonsten wurde absichtlich auf
weitere Libraries verzichtet.

Als Datenbankschnittstelle habe ich mich für PDO
entschieden da die Aufgabe nur wenige Query's erfordert. 
ORM's wie Doctrine oder Eloquent von Laravel würden 
sicherlich den Rahmen sprängen.

Weitere Provider können unter `libs/MB/provider/` angelegt
werden. Diese müssen das Interface `IDatenbank` implementieren.

Da ich kein Routing verwende, sind aktuell in der `index.php`
ein paar Funktionen die einzelne Pages darstellen.

Die Deutsche Namensgebung der Interfaces habe ich behalten da die
Interfaces die Grundlage der Aufgabe darstellen und ich
diese nicht verändern wollte.

## Entwicklungsumgebung

Es wird Docker Compose verwendet um einen
Web Server mit PHP 7 und MariaDB zu betreiben. Zusätzlich
wird der Container `Adminer` gestartet um auf die
MySQL DB zugreifen zu können. Dies geschieht aktuell
über den Port `8180`.

Der folgende Befehl sollte reichen um die Entwicklungsumgebung
zu starten. Danach ist der Login über `localhost`
erreichbar.

```
$ docker-compose up -d
```

## Mögliche Fehlerquellen

Sollte Docker nicht korrekt starten oder die Webseite
nicht angezeigt werden, könnte dies an unterschiedlichen
rechten zwischen Host und Gast liegen.

Hierzu kann in der Datei `php/init.sh` der `chown` Befehl
angepasst werden.

Danach muss der Container mit `docker-compose build php`
neu erstellt werden.