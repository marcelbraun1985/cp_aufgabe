#!/usr/bin/env bash

chown -R www-data:100 /var/www
#chown -R www-data:www-data /var/www/*
chmod -R 775 /var/www/*

a2enmod rewrite

/etc/init.d/apache2 start

tail -f /dev/null
